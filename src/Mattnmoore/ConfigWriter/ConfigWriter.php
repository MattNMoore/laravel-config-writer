<?php namespace Mattnmoore\ConfigWriter;

use Illuminate\Config\FileLoader;
use Illuminate\Config\Repository;
use Illuminate\Foundation\Application;

class ConfigWriter {

	private $configRepository;

	private $fileLoader;

	private $app;

	private $config;

	private $file;

	function __construct(Repository $configRepository, Application $app)
	{
		$this->app = $app;

		$this->configRepository = $configRepository;

		$this->fileLoader = $configRepository->getLoader();
	}

	private function generateConfigFile($config, $string = '', $level = 1)
	{
		if(strlen($string) == 0)
		{
			$string = '<?php' . PHP_EOL . PHP_EOL;
			$string = $string . 'return [' . PHP_EOL;
		}

		foreach($config as $key => $value)
		{
			if(is_array($value))
			{
				$string = $this->indentString($string, $level);
				$string = $string . "'" . $key . "' => [" . PHP_EOL;
				$string = $this->generateConfigFile($value, $string, $level + 1);
			}
			else
			{
				$string = $this->indentString($string, $level);
				$string = $string . "'" . $key . "' => '" . $value . "'," .PHP_EOL;
			}
		}

		$string = $string . PHP_EOL;
		$string = $this->indentString($string, $level - 1);
		$string = $string . '],' . PHP_EOL;

		return $string;
	}

	private function indentString($string, $level)
	{
		foreach(range(1, $level * 4) as $level)
		{
			$string = $string . ' ';
		}

		return $string;
	}

	function getConfig($name, $namespace = 'default')
	{
		$this->config = $this->configRepository->get($namespace . '::' . $name);
		$this->config['themes']['active'] = 'foo';

		$namespaces = $this->fileLoader->getNamespaces();
		$path = $namespaces[$namespace] . '/' . $name . '.php';

		$string = '<?php' . PHP_EOL . PHP_EOL;

		$file = $this->generateConfigFile($this->config);

		$file = $file . PHP_EOL . '];';

		dd($file);

		file_put_contents($path, '<?php return ' . var_export($this->config, true) . ';');

		dd($this->config);




		$this->file = readfile($path);
		dd($this->file);
		dd($this->file);



		dd($path);

		dd($this->file);
		dd($this->fileLoader->exists($name, $namespace));
		echo 'Set config <br />';

		return $this;
	}

	function addToKey($key, $value)
	{
		echo 'Add to key <br />';

		return $this;
	}

	function removeFromKey($key, $value)
	{
		echo 'Remove from key <br />';

		return $this;
	}

	function addKey($key)
	{
		echo 'Add key <br />';

		return $this;
	}

	function removeKey($key)
	{
		echo 'Remove key <br />';

		return $this;
	}

}